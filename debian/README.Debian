autodocktools and other modules of MGLtools for Debian
======================================================

This package aims at providing the complete infrastructure for ligand
screening with Debian Linux.  It is part of the MGL (Molecular Graphics
Labs at Scripps) Python tools set for the computational and visual
interaction of protein structures and their ligands.

The Debian community shall thank Sargis Dallakyan <sargis@scripps.edu>
for his help in easing the compatibility with Debian by accepting our
patches and providing more himself. Also, the leader of the Molecular
Graphics Labs, Prof. Michael Sanner, is thanked for his support.

Please be continuously reminded that that closer a software gets towards
identifying or further describing functional compounds in medicine,
the harder it is to get access to free (as in beer) solutions. And it
certainly does not get much closer than with the AutoDockToolkit. To
have the AutoDockToolkit with Debian is special and good for the world
as it spawns the development of new drugs.  We will need to wait a bit
longer for a DFSG compatible license, though.

If you are a regular user of this package, your feedback to us
packagers would much appreciated. The packaging is organised
via the Debian-Med alioth project that has its own portal on
http://debian-med.alioth.debian.org.

 -- Steffen Moeller <moeller@debian.org>  Sun, 20 Jul 2008 00:33:11 +0200
